package ga.manufakturaczarnoziem.productpricemonitor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Entity
@Table(name="WEBSTORE")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Webstore implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "label")
    private String label;

    @Column(name = "product_url_pattern")
    private String productUrlPattern;

    @Column(name = "product_data_format")
    private String productDataFormat;

    @JsonIgnore
    @OneToMany(mappedBy = "webstore", fetch = FetchType.EAGER)
    private List<ProductPriceSelector> selectors;

    @OneToMany(mappedBy = "webstore", fetch = FetchType.LAZY)
    private List<Product> products;
}
