package ga.manufakturaczarnoziem.productpricemonitor;

import ga.manufakturaczarnoziem.productpricemonitor.exception.AsyncTaskException;
import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import ga.manufakturaczarnoziem.productpricemonitor.model.ProductPrice;
import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import ga.manufakturaczarnoziem.productpricemonitor.service.ProductPriceFetchService;
import ga.manufakturaczarnoziem.productpricemonitor.service.ProductService;
import ga.manufakturaczarnoziem.productpricemonitor.service.WebstoreService;
import ga.manufakturaczarnoziem.productpricemonitor.utils.CustomLogger;
import ga.manufakturaczarnoziem.productpricemonitor.utils.TimeStatistics;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class ScheduledTasks {

    private final CustomLogger clog = new CustomLogger(ScheduledTasks.class);

    private TimeStatistics tStats = new TimeStatistics("Average feeding time: {value} ms");

    @Autowired
    private WebstoreService webstoreService;

    @Autowired
    private ProductPriceFetchService productPriceFetchService;

    @Autowired
    private ProductService productService;


    @Scheduled(cron = "${app.task.feed-product-price.cron}")
    public void feedProductPriceTask() {
        List<CompletableFuture<ProductPrice>> completableFetchedProductPrices = new ArrayList<>();

        Long t = System.currentTimeMillis();

        for (Webstore webstore : webstoreService.getAllWebstoresWithProducts()) {
            for (Product product : webstore.getProducts()) {
                completableFetchedProductPrices.add(productPriceFetchService.asyncFetchProductPrice(webstore, product));
            }
        }

        List<ProductPrice> productPrices = new ArrayList<>();
        try {
            for (CompletableFuture<ProductPrice> completableFetchedProductPrice : completableFetchedProductPrices) {
                productPrices.add(completableFetchedProductPrice.get());
            }
        } catch (InterruptedException | ExecutionException ex) {
            throw new AsyncTaskException("ERROR:", ex.getCause());
        }

        productService.saveAllProductPrices(productPrices);

        tStats.add(System.currentTimeMillis() - t);
        clog.time("Feeding product prices completed in ", t);
        tStats.avgLog();
    }
}
