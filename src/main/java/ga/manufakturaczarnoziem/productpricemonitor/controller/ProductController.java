package ga.manufakturaczarnoziem.productpricemonitor.controller;

import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import ga.manufakturaczarnoziem.productpricemonitor.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping(value = "/api/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = GET, value= "/{id}")
    public Product getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    @RequestMapping(method = POST, value= "/")
    public Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @RequestMapping(method = DELETE, value= "/{id}")
    public void deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

    @RequestMapping(method = GET, value= "/{id}/current")
    public Product getProductWithCurrentPrice(@PathVariable Long id) {
        return productService.getProductWithCurrentPrice(id);
    }

    @RequestMapping(method = GET, value= "/{id}/latest")
    public Product getLatestProductPrices(@PathVariable Long id, @RequestParam(required = false) Integer quantity) {
        return productService.getLatestProductPrices(id, quantity);
    }
}
