package ga.manufakturaczarnoziem.productpricemonitor.controller;

import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import ga.manufakturaczarnoziem.productpricemonitor.service.WebstoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping(value = "/api/webstore", produces = MediaType.APPLICATION_JSON_VALUE)
public class WebstoreController {

    @Autowired
    private WebstoreService webstoreService;


    @RequestMapping(method = GET, value= "/{id}")
    public Webstore getWebstore(@PathVariable Long id) {
        return webstoreService.getWebstore(id);
    }
}
