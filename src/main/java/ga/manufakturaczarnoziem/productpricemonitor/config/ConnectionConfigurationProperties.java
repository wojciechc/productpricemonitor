package ga.manufakturaczarnoziem.productpricemonitor.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("connection")
@Getter
@Setter
public class ConnectionConfigurationProperties {

    Proxy proxy = new Proxy();

    Http http = new Http();

    @Getter
    @Setter
    public static class Proxy {
        private Boolean enabled;
        private String address;
        private Integer port;
    }

    @Getter
    @Setter
    public static class Http {
        private String userAgent;
    }
}
