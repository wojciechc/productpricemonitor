package ga.manufakturaczarnoziem.productpricemonitor.exception;


public class DocumentContentException extends RuntimeException {

    public DocumentContentException(String message) {
        super(message);
    }

    public DocumentContentException(String message, Throwable cause) {
        super(message, cause);
    }
}
