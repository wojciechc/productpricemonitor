package ga.manufakturaczarnoziem.productpricemonitor.exception;


public class AsyncTaskException extends RuntimeException {

    public AsyncTaskException(String message) {
        super(message);
    }

    public AsyncTaskException(String message, Throwable cause) {
        super(message, cause);
    }
}
