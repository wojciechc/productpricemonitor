package ga.manufakturaczarnoziem.productpricemonitor.exception;


public class WebsiteContentFetchException extends RuntimeException {

    public WebsiteContentFetchException(String message) {
        super(message);
    }

    public WebsiteContentFetchException(String message, Throwable cause) {
        super(message, cause);
    }
}
