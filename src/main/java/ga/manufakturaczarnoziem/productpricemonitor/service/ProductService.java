package ga.manufakturaczarnoziem.productpricemonitor.service;

import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import ga.manufakturaczarnoziem.productpricemonitor.model.ProductPrice;
import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import ga.manufakturaczarnoziem.productpricemonitor.repository.ProductPriceRepository;
import ga.manufakturaczarnoziem.productpricemonitor.repository.ProductRepository;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ProductPriceRepository productPriceRepository;

    @Autowired
    private ProductPriceFetchService productPriceFetchService;


    public Product getProduct(Long id) {
        return repository.findOne(id);
    }

    public Product addProduct(Product product) {
        return repository.save(product);
    }

    public void deleteProduct(Long id) {
        Product product = repository.findOne(id);
        if (product != null) {
            repository.delete(product);
        }
    }

    public Product getProductWithCurrentPrice(Long id) {
        Product product = repository.findOne(id);
        Webstore webstore = product.getWebstore();

        product.setProductPrices(Arrays.asList(productPriceFetchService.fetchProductPrice(
                webstore.getProductUrlPattern(),
                webstore.getProductDataFormat(),
                product.getWebstoreProductId(),
                webstore.getSelectors()
        )));

        return product;
    }

    public Product getLatestProductPrices(Long id) {
        return getLatestProductPrices(id, null);
    }

    public Product getLatestProductPrices(Long id, Integer quantity) {
        Product product = repository.findOne(id);

        product.setProductPrices(productPriceRepository.findByProductOrderByDateDesc(product, new PageRequest(0, quantity != null ? quantity : 1)));

        return product;
    }

    public void saveAllProductPrices(List<ProductPrice> productPrices) {
        productPriceRepository.save(productPrices);
    }
}
