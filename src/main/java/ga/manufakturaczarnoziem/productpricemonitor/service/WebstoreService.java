package ga.manufakturaczarnoziem.productpricemonitor.service;

import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import ga.manufakturaczarnoziem.productpricemonitor.repository.WebstoreRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WebstoreService {

    @Autowired
    private WebstoreRepository repository;


    public Webstore getWebstore(Long id) {
        return repository.findOne(id);
    }

    @Transactional
    public List<Webstore> getAllWebstoresWithProducts() {
        List<Webstore> webstores = repository.findAll();
        for (Webstore webstore : webstores) {
            webstore.getProducts().size();
        }

        return webstores;
    }

}
