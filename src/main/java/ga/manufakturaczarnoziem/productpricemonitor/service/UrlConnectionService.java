package ga.manufakturaczarnoziem.productpricemonitor.service;

import ga.manufakturaczarnoziem.productpricemonitor.config.ConnectionConfigurationProperties;
import ga.manufakturaczarnoziem.productpricemonitor.exception.ConnectionException;
import ga.manufakturaczarnoziem.productpricemonitor.exception.WebsiteContentFetchException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UrlConnectionService {

    private final Boolean IS_PROXY_ENABLED;
    private final String PROXY_ADDRESS;
    private final Integer PROXY_PORT;
    private final String HTTP_USERAGENT;

    @Autowired
    public UrlConnectionService(ConnectionConfigurationProperties connectionProps) {
        this.IS_PROXY_ENABLED = connectionProps.getProxy().getEnabled();
        this.PROXY_ADDRESS = connectionProps.getProxy().getAddress();
        this.PROXY_PORT = connectionProps.getProxy().getPort();
        this.HTTP_USERAGENT = connectionProps.getHttp().getUserAgent();
    }


    public String getContentOfUrl(String url) {
        StringBuffer result = new StringBuffer();
        InputStream inputStream = null;
        HttpURLConnection urlConnection = null;

        try {
            URL webUrl = new URL(url);

            if (IS_PROXY_ENABLED) {
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY_ADDRESS, PROXY_PORT));
                urlConnection = (HttpURLConnection)webUrl.openConnection(proxy);
            } else {
                urlConnection = (HttpURLConnection)webUrl.openConnection(Proxy.NO_PROXY);
            }

            urlConnection.addRequestProperty("User-Agent", HTTP_USERAGENT);
            urlConnection.connect();

            String line = null;
            inputStream = urlConnection.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = in.readLine()) != null) {
              result.append(line);
            }

            if (result.length() == 0) {
                throw new WebsiteContentFetchException("ERROR: URL: '" + url + "' returned no content with code: " + urlConnection.getResponseCode());
            }

        } catch (Exception ex) {
            throw new ConnectionException("ERROR: URL: '" + url + "'.\nCAUSE: ", ex.getCause());

        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ioe) {
                throw new ConnectionException("ERROR: URL: '" + url + "'. Cannot close InputStream.\nCAUSE: ", ioe.getCause());
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return result.toString();
    }
}
