package ga.manufakturaczarnoziem.productpricemonitor.service;

import ga.manufakturaczarnoziem.productpricemonitor.exception.DocumentContentException;
import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import ga.manufakturaczarnoziem.productpricemonitor.model.ProductPrice;
import ga.manufakturaczarnoziem.productpricemonitor.model.ProductPriceSelector;
import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import ga.manufakturaczarnoziem.productpricemonitor.utils.HtmlDocumentParser;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class ProductPriceFetchService {

    @Autowired
    private UrlConnectionService urlConnectionService;


    public ProductPrice fetchProductPrice(String productUrlPattern, String productDataFormat, Long webstoreProductId, List<ProductPriceSelector> selectors) {
        return fetchProductPrice(productUrlPattern, productDataFormat, webstoreProductId, getSelectorsAsMap(selectors));
    }

    public ProductPrice fetchProductPrice(String productUrlPattern, String productDataFormat, Long webstoreProductId, Map<String, String> selectors) {
        String url = productUrlPattern.replace("{productId}", webstoreProductId.toString());
        String websiteContent = urlConnectionService.getContentOfUrl(url);

        String value = null;
        switch (productDataFormat) {
            case "HTML":
                if (selectors.containsKey("id") && selectors.containsKey("attributeName")) {
                    value = getValueOfAttributeByElementId(websiteContent, selectors.get("id"), selectors.get("attributeName"));
                }
                break;

            case "JSON"://for future use
                break;
        }
        if (value == null) throw new DocumentContentException("ERROR: Unhandled document content");

        return new ProductPrice(null, null, LocalDateTime.now(), Double.parseDouble(value.trim()));
    }

    @Async("feedProductPriceTask")
    public CompletableFuture<ProductPrice> asyncFetchProductPrice(Webstore webstore, Product product) {
        ProductPrice productPrice = fetchProductPrice(webstore.getProductUrlPattern(), webstore.getProductDataFormat(), product.getWebstoreProductId(), webstore.getSelectors());

        if (productPrice != null) {
            productPrice.setProduct(product);
        }
        return CompletableFuture.completedFuture(productPrice);
    }

    private String getValueOfAttributeByElementId(String websiteContent, String id, String attributeName) {
        return HtmlDocumentParser.getAttributeValueOfElementById(HtmlDocumentParser.getDocument(websiteContent), id, attributeName);
    }

    private Map getSelectorsAsMap(List<ProductPriceSelector> productPriceSelectors) {
        Map<String, String> selectors = new HashMap<>();
        for (ProductPriceSelector productPriceSelector : productPriceSelectors) {
            selectors.put(productPriceSelector.getName(), productPriceSelector.getValue());
        }

        return selectors;
    }
}
