package ga.manufakturaczarnoziem.productpricemonitor;

import java.util.concurrent.Executor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@SpringBootApplication
@EnableScheduling
@EnableAsync
public class ProductPriceMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductPriceMonitorApplication.class, args);
    }

    @Bean(name = "feedProductPriceTask")// TODO: get params from config
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(8);
        executor.setMaxPoolSize(8);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("feedProductPriceTask-");
        executor.initialize();
        return executor;
    }
}