package ga.manufakturaczarnoziem.productpricemonitor.utils;

import ga.manufakturaczarnoziem.productpricemonitor.exception.HtmlParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


public class HtmlDocumentParser {

    public static Document getDocument(String htmlString) {
        return Jsoup.parse(String.valueOf(htmlString));
    }

    public static String getContentOfElementById(Document doc, String elementId) {
        Element element = doc.body().getElementById(elementId);
        if (element == null) throw new HtmlParseException("ERROR: Element with id: '" + elementId + "' not found in document: '" + doc.title() + "'");

        return element.text();
    }

    public static String getAttributeValueOfElementById(Document doc, String elementId, String attributeName) {
        Element element = doc.body().getElementById(elementId);
        if (element == null) throw new HtmlParseException("ERROR: Element with id: '" + elementId + "' not found in document: '" + doc.title() + "'");

        return element.attr(attributeName);
    }
}
