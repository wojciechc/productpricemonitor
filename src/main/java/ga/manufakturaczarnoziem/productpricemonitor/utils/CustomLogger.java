package ga.manufakturaczarnoziem.productpricemonitor.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CustomLogger {

    private final Logger log;


    public CustomLogger(Class clazz) {
        log = LoggerFactory.getLogger(clazz);
    }


    public Long time(String message, Long time) {
        Long logTime = System.currentTimeMillis();
        log.debug("[TIME] " + message + (logTime - time) + "ms");

        return logTime;
    }
}
