package ga.manufakturaczarnoziem.productpricemonitor.utils;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@AllArgsConstructor
public class TimeStatistics extends ArrayList<Long> {

    private final String message;

    public void avgLog() {
        log.debug("[TIME_STAT] " + message.replace("{value}", (this.stream().mapToLong(Long::longValue).sum() / this.size()) + ""));
    }
}
