package ga.manufakturaczarnoziem.productpricemonitor.repository;

import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {

}
