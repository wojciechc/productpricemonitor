package ga.manufakturaczarnoziem.productpricemonitor.repository;

import ga.manufakturaczarnoziem.productpricemonitor.model.Product;
import ga.manufakturaczarnoziem.productpricemonitor.model.ProductPrice;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductPriceRepository extends JpaRepository<ProductPrice, Long> {

    public List<ProductPrice> findByProduct(Product product);

    public List<ProductPrice> findByProductOrderByDateDesc(Product product, Pageable pageable);
}
