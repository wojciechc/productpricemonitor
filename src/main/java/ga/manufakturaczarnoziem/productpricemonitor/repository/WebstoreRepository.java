package ga.manufakturaczarnoziem.productpricemonitor.repository;

import ga.manufakturaczarnoziem.productpricemonitor.model.Webstore;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WebstoreRepository extends JpaRepository<Webstore, Long> {

}
